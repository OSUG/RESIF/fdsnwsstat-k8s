Deploy with helm

    helm secrets upgrade -i staging-fdsnwsstat fdsnwsstat -f helm-values/staging/values.yaml -f helm-values/staging/secrets.yaml 
    helm secrets upgrade -i staging-ph5-fdsnwsstat fdsnwsstat -f helm-values/staging-ph5/values.yaml -f helm-values/staging-ph5/secrets.yaml 

Dans le répertoire fdsnwsstat-redis-k8s, on a les recettes de déploiement de la partie redis qui peut être commune à toutes les instances de fdsnwsstat.

