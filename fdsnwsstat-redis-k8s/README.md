Déploiement d'un service redis à utiliser avec fdsnwsstat

Il dépend d'une recette de déploiement de bitnami et contient la configuration qui convient pour chaque contexte.

    helm repo add bitnami  https://charts.bitnami.com/bitnami 
    cd fdsnwsstat-redis
    helm dependency build
    cd ..
    helm upgrade staging-fdsnwsstat-redis fdsnwsstat-redis -f helm-values/staging/values.yaml
